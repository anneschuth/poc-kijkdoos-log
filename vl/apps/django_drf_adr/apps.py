from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'vl.apps.django_drf_adr'
