import os
import re

from django.conf import settings
from django.http import HttpResponseNotAllowed
from django.utils.deprecation import MiddlewareMixin


class HeaderMiddleware(MiddlewareMixin):

    def process_response(self, request, response):
        # API Design Rule: API-03: Only apply standard HTTP methods.
        permitted_methods = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE',
                             'OPTIONS']  # DRF uses OPTIONS.
        if request.method not in permitted_methods:
            return HttpResponseNotAllowed(permitted_methods=permitted_methods)

        # API Design Rule: API-57: Return the full version number in a response header
        response['API-Version'] = settings.API_VERSION_SEMVER

        # API Design Rule: API-51: Publish OAS document at a standard location in JSON-format.
        # NOTE: You still have to implement drf_spectacular to add openapi.json/openapi.yaml auto generated schema endpoints.
        if request.headers.get("origin"):
            if request.resolver_match is not None:
                pattern = r'^[\w\-./]+(/openapi\.json|/openapi\.yaml)$'
                if re.match(pattern, request.resolver_match.route):
                    response['Access-Control-Allow-Origin'] = '*'

        return response
