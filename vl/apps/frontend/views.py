import json
from urllib.parse import urlencode
from datetime import datetime
from django.views.generic import TemplateView
from rest_framework.test import APIRequestFactory

from vl.apps.api import views


class IndexView(TemplateView):
    def get_template_names(self):
        # If the request was made with HTMX, use only the partial with the results. Otherwise use the full template
        if self.request.headers.get("HX-Request") == "true":
            return ["partials/results.html"]

        return ["index.html"]

    def get_context_data(self, **kwargs):
        # Fetch the data from the backend. IMPROVE: fetch the data from the backend output instead? Note: we could also send requests directly from JavaScript, but we prefer not to use a frontend framework
        view = views.VerwerkingsActieViewSet.as_view({"get": "list"})
        factory = APIRequestFactory()

        params = {}
        if system := self.request.GET.get("system"):
            params["systeem__iexact"] = system
        if activity := self.request.GET.get("activity"):
            params["verwerkings_activiteit__iexact"] = activity
        if date_from := self.request.GET.get("from"):
            params["created__gte"] = datetime.strptime(
                date_from, "%d-%m-%Y %H:%M"
            ).isoformat(timespec="seconds")
        if date_to := self.request.GET.get("to"):
            params["created__lte"] = datetime.strptime(
                date_to, "%d-%m-%Y %H:%M"
            ).isoformat(timespec="seconds")

        data = view(factory.get(f"?{urlencode(params)}")).data

        # Get the lists of available options for the filters
        systems = "[]"
        activities = "[]"

        if "results" in data:
            # In the data, parse the 'created' attributes as datetime
            for res in data["results"]:
                res["created"] = datetime.fromisoformat(res["created"])

            systems = json.dumps(
                [{"id": "", "label": "Alle systemen"}]
                + [
                    {"id": sys, "label": sys}
                    for sys in sorted(
                        list(
                            set(
                                [
                                    item["systeem"]
                                    for item in data["results"]
                                    if item["systeem"] is not None
                                ]
                            )
                        )
                    )
                ]
            )

            activities = json.dumps(
                [{"id": "", "label": "Alle verwerkingsactiviteiten"}]
                + [
                    {"id": act, "label": act}
                    for act in sorted(
                        list(
                            set(
                                [
                                    item["verwerkings_activiteit"]
                                    for item in data["results"]
                                    if item["verwerkings_activiteit"] is not None
                                ]
                            )
                        )
                    )
                ]
            )

        current_date_range = self.request.GET.get("from")
        if current_to := self.request.GET.get("to"):
            current_date_range += " - " + current_to

        return super().get_context_data(
            currentSystem=self.request.GET.get("system"),
            currentActivity=self.request.GET.get("activity"),
            current_date_range=current_date_range,
            data=data,
            systems=systems,
            activities=activities,
            **kwargs,
        )
