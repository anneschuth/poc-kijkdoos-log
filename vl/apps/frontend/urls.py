from django.urls import path
from django.views.generic.base import RedirectView

from .views import IndexView

urlpatterns = (
    # security.txt redirect. Note: 302 redirect by default. Set permanent=True to change to 301
    path(
        ".well-known/security.txt",
        RedirectView.as_view(url="https://www.ncsc.nl/.well-known/security.txt"),
    ),

    path('', IndexView.as_view())
)
