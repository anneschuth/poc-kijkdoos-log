from django.apps import AppConfig


class APIConfig(AppConfig):
    name = 'vl.apps.frontend'
