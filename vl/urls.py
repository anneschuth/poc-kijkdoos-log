from django.contrib import admin
from django.urls import (
    include,
    path,
)

urlpatterns = (
    path('api/', include('vl.apps.api.urls')),
    path('admin/', admin.site.urls),
    path('', include('vl.apps.frontend.urls')),
)
