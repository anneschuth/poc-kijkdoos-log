import htmx from 'htmx.org';

import { TempusDominus } from '@eonasdan/tempus-dominus';


// Common web components that are used on several pages. Note: intentionally written in vanilla JS, since this is preferred over a framework with possible vendor lock-in

// VLDropdown is a dropdown with filter
class VLDropdown extends HTMLElement {
  constructor() {
    // Always call super first in the constructor
    super();

    // Set properties
    this.isOpen = false;
    this.value = this.getAttribute('value');

    // Create a wrapper div with position: relative
    const wrap = document.createElement('div');
    wrap.className = 'block sm:inline-block relative sm:mr-2 mb-2';

    // Create a button element
    const button = document.createElement('button');
    button.setAttribute('type', 'button');
    button.setAttribute('id', this.getAttribute('id'));
    this.removeAttribute('id'); // Remove the `id` attribute from the Web Component, so the browser knows which element to focus when a label for it is clicked
    button.className = 'w-full xs:w-auto border border-gray-300 text-gray-800 bg-white text-sm rounded-lg focus:outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 p-2.5 flex items-center shadow-sm';
    button.innerHTML = `<span class="grow text-left sm:w-48 whitespace-nowrap text-ellipsis overflow-hidden text-gray-400">${this.getAttribute('placeholder')}</span> <svg class="w-4 h-4 ml-2" aria-hidden="true" fill="none" stroke="currentColor"
        viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
      </svg>`;

    // Create a hidden input, so the element can be used in regular forms. Note: using a hidden input, since modifying formData of this.closest('form') on submit seems to be difficult to combine with HTMX. Support for the ElementInternals interface (see https://webkit.org/blog/13711/elementinternals-and-form-associated-custom-elements/) is currently still limited
    const hidden = document.createElement('input');
    hidden.setAttribute('type', 'hidden');
    hidden.setAttribute('name', this.getAttribute('name'));
    hidden.setAttribute('value', this.value);

    // Create the menu element
    this.menu = document.createElement('div');
    this.menu.className = 'hidden absolute left-0 z-10 bg-white rounded shadow border border-gray-300 divide-y divide-gray-200';

    // Create the filter input element
    this.filter = document.createElement('input');
    this.filter.setAttribute('type', 'text');
    this.filter.setAttribute('placeholder', 'Filter…');
    this.filter.className = 'text-gray-800 bg-transparent text-sm block w-full px-4 py-3 border-0 focus:outline-none placeholder:text-gray-400 focus:ring-0';

    // Create an element that contains the dropdown items
    const list = document.createElement('ul');
    list.className = 'py-2 text-sm text-gray-700 max-h-60 overflow-y-auto';

    // Create the list items
    const span = button.querySelector('span');
    const items = JSON.parse(this.getAttribute('choices'));
    const val = this.getAttribute('value');
    items.forEach(item => {
      const el = document.createElement('li');
      el.className = 'block px-4 py-2 hover:bg-gray-100 cursor-pointer';
      el.innerText = item.label;
      list.appendChild(el);

      // In case a value is selected, set it in the button span
      if (item.id === val) {
        span.innerText = item.label;
        span.classList.remove('text-gray-400'); // Remove the gray text color
      }

      // Add a click event to the item element
      el.addEventListener('click', () => {
        this.value = item.id;
        span.innerText = item.label;
        span.classList.remove('text-gray-400'); // Remove the gray text color
        hidden.setAttribute('value', this.value);

        this.close();

        // Fire a 'change' event
        this.dispatchEvent(new Event('change'));
      });
    });

    // Append and insert the elements
    this.menu.replaceChildren(this.filter, list);
    wrap.replaceChildren(button, hidden, this.menu);
    this.replaceChildren(wrap);

    // Add events
    button.addEventListener('click', (e) => {
      if (this.isOpen) {
        this.close();
      } else {
        this.open();
      }

      e.stopPropagation();
    })

    // Close the dropdown when clicked outside
    this.handleClick = ((e) => {
      if (!(this.menu.contains(e.target))) {
        this.close();
      }
    }).bind(this);
    document.addEventListener('click', this.handleClick);

    // Close the dropdown when the Esc key is pressed
    this.handleKeyDown = ((e) => {
      if (e.key === 'Escape' || e.key === 'Esc') {
        this.close();
      }
    }).bind(this);
    document.addEventListener('keydown', this.handleKeyDown);

    // Filter events
    this.filter.addEventListener('input', () => {
      const query = this.filter.value.trim().toLowerCase();

      Array.from(list.children).forEach(el => {
        if (el.innerText.toLowerCase().includes(query)) {
          el.classList.remove('hidden');
        } else {
          el.classList.add('hidden');
        }
      });

      // In case of 0 results, show an element that tells this
      // TODO
    });


    // Support up and down arrows, prevent default behaviour
    // TODO
  }

  open() {
    this.menu.classList.remove('hidden');
    this.isOpen = true;

    this.filter.focus();
    this.filter.select();

    // IMPROVE: set z-index of the menu higher than currently open dropdown menus and datepickers, or close other dropdowns
  }

  close() {
    this.menu.classList.add('hidden');
    this.isOpen = false;
  }

  // // connectedCallback runs each time the element is appended to or moved in the DOM
  // connectedCallback() {
  //   console.log('connected!', this);
  // }

  // disconnectedCallback runs when the element is removed from the DOM
  disconnectedCallback() {
    // Remove the click and keydown events from the document
    document.removeEventListener('click', this.handleClick);
    document.removeEventListener('keydown', this.handleKeyDown);
  }
}


const datepickerOptions = {
  dateRange: true,
  multipleDatesSeparator: ' - ',
  display: {
    theme: 'light',
    icons: {
      type: 'sprites',
      time: '/static/img/feather-sprite.svg#clock',
      date: '/static/img/feather-sprite.svg#calendar',
      up: '/static/img/feather-sprite.svg#arrow-up',
      down: '/static/img/feather-sprite.svg#arrow-down',
      previous: '/static/img/feather-sprite.svg#arrow-left',
      next: '/static/img/feather-sprite.svg#arrow-right',
      clear: '/static/img/feather-sprite.svg#x',
    },
    buttons: {
      today: false,
      clear: true,
      close: false,
    }
  },
  localization: {
    // today: 'Go to today',
    // clear: 'Clear selection',
    // close: 'Close the picker',
    // selectMonth: 'Select Month',
    // previousMonth: 'Previous Month',
    // nextMonth: 'Next Month',
    // selectYear: 'Select Year',
    // previousYear: 'Previous Year',
    // nextYear: 'Next Year',
    // selectDecade: 'Select Decade',
    // previousDecade: 'Previous Decade',
    // nextDecade: 'Next Decade',
    // previousCentury: 'Previous Century',
    // nextCentury: 'Next Century',
    // pickHour: 'Pick Hour',
    // incrementHour: 'Increment Hour',
    // decrementHour: 'Decrement Hour',
    // pickMinute: 'Pick Minute',
    // incrementMinute: 'Increment Minute',
    // decrementMinute: 'Decrement Minute',
    // pickSecond: 'Pick Second',
    // incrementSecond: 'Increment Second',
    // decrementSecond: 'Decrement Second',
    // toggleMeridiem: 'Toggle Meridiem',
    // selectTime: 'Select Time',
    // selectDate: 'Select Date',
    dayViewHeaderFormat: { month: 'long', year: 'numeric' },
    locale: 'default',
    startOfTheWeek: 1,
    hourCycle: 'h23',
    dateFormats: {
      LTS: 'HH:mm:ss',
      LT: 'HH:mm',
      L: 'dd-MM-yyyy',
      LL: 'd MMMM, yyyy',
      LLL: 'd MMMM, yyyy HH:mm',
      LLLL: 'dddd, d MMMM, yyyy HH:mm'
    },
  }
};

// VLDateTimePicker consists of a text input with datetimepicker
class VLDateTimePicker extends HTMLElement {
  constructor() {
    // Always call super first in the constructor
    super();

    // Set properties
    this.value = this.getAttribute('value') || '';

    // Create a wrapper div
    const wrap = document.createElement('div');
    wrap.className = 'block sm:inline-block relative mb-6';

    // Create an icon div element
    const icon = document.createElement('div');
    icon.className = 'absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none';
    icon.innerHTML = `<svg aria-hidden="true" class="w-5 h-5 text-gray-500" fill="currentColor" viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd"
            d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z"
            clip-rule="evenodd"></path>
        </svg>`;

    // Create an input element
    const input = document.createElement('input');
    input.setAttribute('id', this.getAttribute('id'));
    this.removeAttribute('id'); // Remove the `id` attribute from the Web Component, so the browser knows which element to focus when a label for it is clicked
    input.setAttribute('name', this.getAttribute('name'));
    input.setAttribute('type', 'text');
    input.setAttribute('value', this.value);
    input.setAttribute('placeholder', this.getAttribute('placeholder'));
    input.setAttribute('autocomplete', 'off');
    input.className = 'bg-white border border-gray-300 text-gray-800 text-sm rounded-lg focus:outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full sm:w-80 p-2.5 pl-10 shadow-sm placeholder:text-gray-400 ' + this.getAttribute('extra-input-classes');

    // Append and insert the elements
    wrap.replaceChildren(icon, input);
    this.replaceChildren(wrap);

    // Initialize the datetimepicker
    new TempusDominus(input, datepickerOptions);

    // Do not let the change event on the input propagate to the Web Component
    input.addEventListener('change', (e) => {
      e.stopPropagation();
    })

    // On hide, fire the change event on the Web Component
    input.addEventListener('hide.td', () => {
      this.value = input.value;
      this.dispatchEvent(new Event('change'));
    })
  }
}


// Set event handlers on the components. Note: before enabling the web components, which is done below
const systemFilter = document.getElementById('system');
const activityFilter = document.getElementById('activity');
const timerangeFilter = document.getElementById('timerange');
const resultsEl = document.getElementById('results');

systemFilter.addEventListener('change', handleFilterChange);
activityFilter.addEventListener('change', handleFilterChange);
timerangeFilter.addEventListener('change', handleFilterChange);

function handleFilterChange() {
  const systemVal = (systemFilter.value || '').trim();
  const activityVal = (activityFilter.value || '').trim();
  let from, to;

  // Split the time range in from and to
  const rangeMatches = timerangeFilter.value.trim().match(/(.+?)( - (.+)|$)/);

  if (rangeMatches) {
    from = rangeMatches[1];
    to = rangeMatches[3];
  }

  // Build the new URL
  let newURL = new URL(location.origin);

  if (systemVal) {
    newURL.searchParams.set('system', systemVal);
  }
  if (activityVal) {
    newURL.searchParams.set('activity', activityVal);
  }
  if (from) {
    newURL.searchParams.set('from', from);
  }
  if (to) {
    newURL.searchParams.set('to', to);
  }

  // Fetch and update the results using HTMX
  htmx.ajax('GET', newURL.toString(), resultsEl);
  history.replaceState({}, "", newURL); // IMPROVE: use pushState and listen to changes
}


// Define the web components
if ('customElements' in window) {
  customElements.define('vl-dropdown', VLDropdown);
  customElements.define('vl-datetimepicker', VLDateTimePicker);
}
