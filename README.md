# Proof of Concept: Verwerkingenlog


## Local

### Init
Create the virtual environment with python dependencies and initialize a local database.
```bash
python -m venv venv
source venv/bin/activate
pip install -r etc/requirements.txt
python vl/manage.py migrate
python vl/manage.py loaddata etc/fixtures-dev.json
```

Make sure pnpm is installed and run:
```bash
pnpm install
```

### Development
Run the development webserver.
```bash
pnpm dev
source venv/bin/activate
python vl/manage.py runserver
```

There are multiple ways to interact with the data from your browser:
- API: Visit http://localhost:8000/api/v0/verwerkingsactie.
- CMS: Visit http://localhost:8000/admin in your browser and use admin/admin to authenticate to use the built-in admin panel.

Or the preferred way which can be used from other apps to leverage the API:
`curl -i -X GET -H 'Content-Type: application/json' http://localhost:8000/api/v0/verwerkingsactie/`

### Test
```bash
source venv/bin/activate
python vl/manage.py test
```


## API usage examples

### Frontend
http://localhost:8000/

### Filtering
http://localhost:8000/api/v0/verwerkingsactie?created__date=2023-10-05
http://localhost:8000/api/v0/verwerkingsactie?created__gte=2023-10-01T00:00:00Z&created__lte=2023-11-01T00:00:00Z
http://localhost:8000/api/v0/verwerkingsactie?verwerkings_activiteit__iexact=https://domain.tld/var/oin/dd940fb2-0a9c-405c-a9c5-2b4e16ec8137
http://localhost:8000/api/v0/verwerkingsactie?verwerkings_activiteit__isnull=True  # This filters the blind spot items.

### Pagination
http://localhost:8000/api/v0/verwerkingsactie?offset=10&limit=5  # Skips the first 10 items and shows the next 5. Defaults to 100 items per page.


## OpenAPI
Open http://localhost:8000/api/v0/openapi.json for a dynamically generated spec. You can swap .json with .yaml as well.


## CI/CD pipeline
Powered by [Tekton](https://gitlab.com/digilab.overheid.nl/platform/flux/core/-/blob/main/docs/run-tekton-locally.md?ref_type=heads).

### Local
See: https://gitlab.com/digilab.overheid.nl/platform/flux/core/-/blob/main/docs/run-tekton-locally.md?ref_type=heads.

Manual docker build: `docker build -f deploy/Dockerfile -t registry.gitlab.com/digilab.overheid.nl/ecosystem/verwerkingenlogging/poc-verwerkingenlog --platform linux/amd64 .`
Manual docker run: `docker run -ti --rm -p 8000:8000 registry.gitlab.com/digilab.overheid.nl/ecosystem/verwerkingenlogging/poc-verwerkingenlog`

### Remote
TODO


## License
Licensed under EUPL v1.2
