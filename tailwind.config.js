/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: 'jit',
  content: [
    './vl/apps/frontend/templates/**/*.html',
    './assets/js/**/*.js',
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}

