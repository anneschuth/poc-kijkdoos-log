# Stage 1
FROM node:20-alpine3.17 AS builder

# Copy the code into the container. Note: copy to a dir instead of `.`, since Parcel cannot run in the root dir, see https://github.com/parcel-bundler/parcel/issues/6578
WORKDIR /build
COPY . .

RUN corepack enable

# Build the static files
RUN corepack enable && corepack prepare pnpm@latest --activate
RUN pnpm install
RUN pnpm build


# Stage 2
FROM     python:3.11

WORKDIR  /app

COPY     ./etc/requirements* /app/etc/

RUN      set -ex \
         && python -m venv /venv \
         && /venv/bin/pip install --upgrade pip \
         && /venv/bin/pip install --no-cache-dir -r /app/etc/requirements.txt \
         && /venv/bin/pip install --no-cache-dir -r /app/etc/requirements-live.txt

COPY     . /app

COPY     --from=builder /build/static /app/static
RUN      /venv/bin/python manage.py collectstatic --no-input

ENV      VIRTUAL_ENV /venv
ENV      PATH /venv/bin:$PATH
ENV      PYTHONUNBUFFERED=1

EXPOSE   8000

CMD      ["gunicorn", "--bind", "0.0.0.0:8000", "--workers", "3", "vl.wsgi:application"]
